import Vue from 'vue'
import App from './App.vue'
// import myTemplate from './lib/index.js'
import myTemplate from '../dist/my'
Vue.use(myTemplate)
console.log(myTemplate)
new Vue({
  el: '#app',
  render: h => h(App)
})
